import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class WashingMachinesProvider {
	List<WashingMachine> washingMachines = new ArrayList<WashingMachine>();

	public void printList(List<WashingMachine> washingMachines) {
		washingMachines.forEach(washingMachine -> System.out.println(washingMachine.toString()));
	}

	public List<WashingMachine> getSorted() {
		return washingMachines.stream()
				.sorted((washingMachine1, washingMachine2) -> washingMachine1.getBrandName()
						.compareTo(washingMachine2.getBrandName()))
				.collect(Collectors.toList());
	}

}
