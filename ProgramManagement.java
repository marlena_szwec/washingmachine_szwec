
public interface ProgramManagement {

	void setProgram(int program);

	int getProgram();

	void nextProgram();

	void previusProgram();

}
