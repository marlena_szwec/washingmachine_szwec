
public interface TemperatureManagement {

	void setTemperture(double temperature);

	double getTemperature();

	void tempUp();

	void tempDown();

}
