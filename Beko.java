
public class Beko extends WashingMachine{
	
	public Beko() {
		super();
		TEMPERATURE_BOUND=1;
	}
	
	@Override
	String getBrandName() {
		return WashingMachineType.BEKO.getBrand();
	}
	
	@Override
	public double getRoundedTemperature(double temperature) {
		return Math.round(temperature);
	}

}
