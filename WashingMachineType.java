
public enum WashingMachineType {
	BEKO("Beko"), WHIRPOOL("Whirpool"), AMICA("Amica");

	private final String brand;

	private WashingMachineType(String brand) {
		this.brand = brand;
	}

	public String getBrand() {
		return this.brand;
	}

}
