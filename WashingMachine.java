public abstract class WashingMachine implements TemperatureManagement, RotationSpeedManagement, ProgramManagement {
	protected int program;
	protected double temperature;
	protected int rotationSpeed;
	protected static final int MIN_PROGRAM = 1;
	protected static int MAX_PROGRAM = 20;
	protected static final double MIN_TEMPERATURE = 0;
	protected static final double MAX_TEMPERATURE = 90;
	protected static final int MIN_ROTATIONSPEED = 0;
	protected static final int MAX_ROTATIONSPEED = 1000;
	protected static double TEMPERATURE_BOUND = 0.5;
	protected static final int ROTATIONSPEED_BOUND = 100;

	public WashingMachine() {
		this.program = 0;
		this.temperature = 0;
		this.rotationSpeed = 0;
	}

	abstract String getBrandName();

	@Override
	public String toString() {
		return "WashingMachine [program=" + program + ", temperature=" + temperature + ", rotationSpeed="
				+ rotationSpeed + ", brandName=" + getBrandName() + "]";
	}

	@Override
	public void setProgram(int program) {
		if (program >= MIN_PROGRAM && program <= MAX_PROGRAM) {
			this.program = program;
		} else {
			throw new WashingMachineException("Program out of range!");
		}
	}

	@Override
	public int getProgram() {
		return this.program;
	}

	@Override
	public void nextProgram() {
		if (program < MAX_PROGRAM) {
			this.program++;
		} else {
			throw new WashingMachineException("The set program is the maximum program!");
		}
	}

	@Override
	public void previusProgram() {
		if (program > MIN_PROGRAM) {
			this.program--;
		} else {
			throw new WashingMachineException("The set program is the minimum program!");
		}
	}

	@Override
	public void setRotationSpeed(int rotationSpeed) {
		if (rotationSpeed >= MIN_ROTATIONSPEED && rotationSpeed <= MAX_ROTATIONSPEED
				&& rotationSpeed % ROTATIONSPEED_BOUND == 0) {
			this.rotationSpeed = rotationSpeed;
		} else {
			throw new WashingMachineException("Rotation speed incorrect or out of range!");
		}
	}

	@Override
	public int getRotationSpeed() {
		return this.rotationSpeed;
	}

	@Override
	public void upRotationSpeed() {
		if (rotationSpeed < MAX_ROTATIONSPEED) {
			rotationSpeed += ROTATIONSPEED_BOUND;
		} else {
			rotationSpeed = MIN_ROTATIONSPEED;
		}
	}

	@Override
	public void downRotationSpeed() {
		if (rotationSpeed > MIN_ROTATIONSPEED) {
			rotationSpeed -= ROTATIONSPEED_BOUND;
		} else {
			rotationSpeed = MAX_ROTATIONSPEED;
		}
	}

	public double getRoundedTemperature(double temperature) {
		return Math.round(temperature * 2) / 2.0;
	}

	@Override
	public void setTemperture(double temperature) {
		if (temperature >= MIN_TEMPERATURE && temperature <= MAX_TEMPERATURE) {
			this.temperature = getRoundedTemperature(temperature);
		} else {
			throw new WashingMachineException("Temperature out of range!");
		}
	}

	@Override
	public double getTemperature() {
		return this.temperature;
	}

	@Override
	public void tempUp() {
		if (temperature < MAX_TEMPERATURE) {
			temperature += TEMPERATURE_BOUND;
			System.out.println("Current temperature " + temperature + "\u00b0C");
		} else {
			throw new WashingMachineException(
					"The temperature can't be increased! The set temperature is the maximum temperature! ");
		}
	}

	@Override
	public void tempDown() {
		if (temperature > MIN_TEMPERATURE) {
			temperature -= TEMPERATURE_BOUND;
			System.out.println("Current temperature " + temperature + "\u00b0C");
		} else {
			throw new WashingMachineException(
					"The temperature can't be reduced! The set temperature is the minimum temperature! ");
		}

	}

	public void showStatus() {
		System.out.println("Brand Name:\t\t" + getBrandName() + "\nProgram:\t\t" + getProgram() + "\nTemperature:\t\t"
				+ getTemperature() + "\u00b0C\nRotation Speed:\t\t " + getRotationSpeed());
	}
}
