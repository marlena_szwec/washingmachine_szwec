import java.util.List;

public class Main {

	public static void main(String[] args) {

		Amica testAmica = new Amica();
		System.out.println("Obiekt klasy Amica :");
		System.out.println(testAmica.toString());
		System.out.println("Zmiana temperatury na 15.67\u00b0C");
		testAmica.setTemperture(15.67);
		System.out.println(testAmica.toString());
		System.out.println("Zmiana temperatury na 91\u00b0C");

		try {
			testAmica.setTemperture(91);
		} catch (WashingMachineException e) {
			System.out.println(e.getMessage());
		}

		System.out.println(testAmica.toString());
		System.out.println("Pobieranie temperatury");
		System.out.println(testAmica.getTemperature());
		System.out.println("Zwiekszanie temperatury");
		testAmica.tempUp();
		System.out.println("Zmniejszanie temperatury");
		testAmica.tempDown();
		System.out.println("Zwiekszanie temperatury przy aktualnej temp.90\u00b0C");
		testAmica.setTemperture(90);

		try {
			testAmica.tempUp();
		} catch (WashingMachineException e) {
			System.out.println(e.getMessage());
		}

		System.out.println("Zmniejszanie temperatury przy aktualnej temp.0\u00b0C");
		testAmica.setTemperture(0);

		try {
			testAmica.tempDown();
		} catch (WashingMachineException e) {
			System.out.println(e.getMessage());
		}

		System.out.println("Ustawienie programu na 15:");
		testAmica.setProgram(15);
		System.out.println(testAmica.toString());
		System.out.println("Ustawienie programu na program spoza dopuszczalnego zakresu (21): ");
		try {
			testAmica.setProgram(21);
		} catch (WashingMachineException e) {
			System.out.println(e.getMessage());
		}
		System.out.println(testAmica.toString());
		System.out.println("Pobranie programu: ");
		System.out.println(testAmica.getProgram());
		System.out.println("Ustawiam nastepny program: ");
		testAmica.nextProgram();
		System.out.println(testAmica.getProgram());
		System.out.println("Ustawiam poprzedni program: ");
		testAmica.previusProgram();
		System.out.println(testAmica.getProgram());
		System.out.println("Ustawiam nastepny program przy aktualnym programie 20: ");
		testAmica.setProgram(20);
		try {
			testAmica.nextProgram();
		} catch (WashingMachineException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Ustawiam poprzedni program przy aktualnym programie 1: ");
		testAmica.setProgram(1);
		try {
			testAmica.previusProgram();
		} catch (WashingMachineException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Ustawiam predkosc wirowania: ");
		testAmica.setRotationSpeed(700);
		System.out.println(testAmica.getRotationSpeed());
		System.out.println("Ustawiam predkosc wirowania: 566 ");
		try {
			testAmica.setRotationSpeed(566);
		} catch (WashingMachineException e) {
			System.out.println(e.getMessage());
		}
		System.out.println(testAmica.getRotationSpeed());
		System.out.println("Zwiekszam predkosc wirowania: ");
		testAmica.upRotationSpeed();
		System.out.println(testAmica.rotationSpeed);
		System.out.println("Zmniejszam predkosc wirowania: ");
		testAmica.downRotationSpeed();
		System.out.println(testAmica.rotationSpeed);
		System.out.println("Zwiekszam predkosc wirowania przy ustawionej maks. predkosci:  ");
		testAmica.setRotationSpeed(1000);
		testAmica.upRotationSpeed();
		System.out.println(testAmica.rotationSpeed);
		System.out.println("Zmniejszam predkosc wirowania przy ustawionej min. predkosci:  ");
		testAmica.setRotationSpeed(0);
		testAmica.downRotationSpeed();
		System.out.println(testAmica.rotationSpeed);

		System.out.println("Wywoluje metode showStatus(): ");
		testAmica.showStatus();

		Beko testBeko = new Beko();
		System.out.println("Ustawiam w pralce Beko temp. na 20.1\u00b0C");
		testBeko.setTemperture(20.1);
		System.out.println(testBeko.toString());
		System.out.println("Ustawiam w pralce Beko temp. na 15.5\u00b0C");
		testBeko.setTemperture(15.5);
		System.out.println(testBeko.toString());
		System.out.println("Zwiekszam temperature w pralce Beko: ");
		testBeko.tempUp();;
		System.out.println("Zmniejszam temperature w pralce Beko: ");
		testBeko.tempDown();;
		

		Whirpool testWhirpool = new Whirpool();
		System.out.println("Ustawiam w pralce Whirpool program na 24:");
		testWhirpool.setProgram(24);
		System.out.println(testWhirpool.toString());
		System.out.println("Proba zwiekszenia programu (aktualny program 25):");
		testWhirpool.setProgram(25);
		try {
			testWhirpool.nextProgram();
		} catch (WashingMachineException e) {
			System.out.println(e.getMessage());
		}
		System.out.println(testWhirpool.toString());

		WashingMachinesProvider washingMachinesProvider = new WashingMachinesProvider();
		washingMachinesProvider.washingMachines.add(testWhirpool);
		washingMachinesProvider.washingMachines.add(testBeko);
		washingMachinesProvider.washingMachines.add(testAmica);

		System.out.println("Drukuje liste : ");
		washingMachinesProvider.printList(washingMachinesProvider.washingMachines);

		System.out.println("Sortuje liste.");
		List<WashingMachine> sortedWashingMachines = washingMachinesProvider.getSorted();

		System.out.println("Drukuje liste po sortowaniu: ");
		washingMachinesProvider.printList(sortedWashingMachines);

	}

}
