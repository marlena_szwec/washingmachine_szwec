
public interface RotationSpeedManagement {

	void setRotationSpeed(int rotationSpeed);

	int getRotationSpeed();

	void upRotationSpeed();

	void downRotationSpeed();
}
