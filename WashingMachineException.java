
public class WashingMachineException extends RuntimeException {

	public WashingMachineException(String message) {
		super(message);
	}

}
