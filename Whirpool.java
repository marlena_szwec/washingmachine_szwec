
public class Whirpool extends WashingMachine {

	public Whirpool() {
		super();
		MAX_PROGRAM = 25;
	}

	@Override
	String getBrandName() {
		return WashingMachineType.WHIRPOOL.getBrand();
	}

}
